package alisson;

public class City {

    private long id;
    private String state;
    private String city;

    public City( long id, String state, String city, String region ) {
        this.id = id;
        this.state = state;
        this.city = city;
        this.region = region;
    }

    public long getId() {
        return id;
    }

    public void setId( long id ) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState( String state ) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    private String region;

    public String getRegion() {
        return region;
    }

    public void setRegion( String region ) {
        this.region = region;
    }
}
