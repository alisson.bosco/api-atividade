package alisson;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CityHandler implements HttpHandler {

    private CityController cityController;

    public CityHandler( CityController cityController ) {
        this.cityController = cityController;
    }

    @Override
    public void handle( HttpExchange exchange ) throws IOException {
        String path = exchange.getRequestURI().getPath();
        String method = exchange.getRequestMethod();

        if ( path.equals( "/cities" ) && method.equalsIgnoreCase( "GET" ) ) {
            handleGetAllCities( exchange );
        } else if ( path.equals( "/cities/filter" ) && method.equalsIgnoreCase( "GET" ) ) {
            handleFilterCities( exchange );
        } else if ( path.equals( "/cities" ) && method.equalsIgnoreCase( "POST" ) ) {
            handleCreateCity( exchange );
        } else {
            sendResponse( exchange, "Endpoint not found", 404 );
        }
    }
    
    private void handleGetAllCities( HttpExchange exchange ) throws IOException {
        List< City > cities = cityController.getAllCities();
        JSONArray jsonCities = new JSONArray();

        for ( City city : cities ) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put( "id", city.getId() );
            jsonObject.put( "state", city.getState() );
            jsonObject.put( "city", city.getCity() );
            jsonObject.put( "region", city.getRegion() );
            jsonCities.put( jsonObject );
        }

        sendJSONResponse( exchange, jsonCities.toString(), 200 );
    }

    private void handleFilterCities( HttpExchange exchange ) throws IOException {
        String state = getQueryParam( exchange, "state" );
        String region = getQueryParam( exchange, "region" );

        List< City > filteredCities = cityController.filterCities( state, region );
        String response = "Filtered Cities:\n";
        for ( City city : filteredCities ) {
            response += city.getState() + " - " + city.getCity() + " - " + city.getRegion() + "\n";
        }
        sendResponse( exchange, response, 200 );
    }

    private void handleCreateCity(HttpExchange exchange) throws IOException {
        String requestBody = getRequestBody(exchange);
        JSONObject jsonObject = new JSONObject(requestBody);
    
        String state = jsonObject.optString("state");
        String city = jsonObject.optString("city");
        String region = jsonObject.optString("region");
    
        cityController.createCity(state, city, region);
        sendResponse(exchange, "City created successfully", 201);
    }
    

    private void sendResponse( HttpExchange exchange, String response, int statusCode ) throws IOException {
        exchange.getResponseHeaders().set( "Content-Type", "text/plain" );
        exchange.sendResponseHeaders( statusCode, response.getBytes().length );
        OutputStream os = exchange.getResponseBody();
        os.write( response.getBytes() );
        os.close();
    }

    private String getQueryParam( HttpExchange exchange, String paramName ) {
        String query = exchange.getRequestURI().getQuery();
        if ( query != null ) {
            String[] queryParams = query.split( "&" );
            for ( String param : queryParams ) {
                String[] keyValue = param.split( "=" );
                if ( keyValue.length == 2 && keyValue[ 0 ].equals( paramName ) ) {
                    return keyValue[ 1 ];
                }
            }
        }
        return null;
    }
    private void sendJSONResponse(HttpExchange exchange, String response, int statusCode) throws IOException {
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        sendResponse(exchange, response, statusCode);
    }

    private String getRequestBody(HttpExchange exchange) throws IOException {
        InputStream inputStream = exchange.getRequestBody();
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString("UTF-8");
    }
    
}
