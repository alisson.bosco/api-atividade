package alisson;

 
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class CityService {
    private final AtomicLong idCounter = new AtomicLong();
    private final List<City> cityList = new ArrayList<>();

    public City createCity(String state, String city, String region) {
        City newCity = new City(idCounter.incrementAndGet(), state, city, region);
        cityList.add(newCity);
        return newCity;
    }

    public List<City> getAllCities() {
        return cityList;
    }

    public List<City> filterCities(String state, String region) {
        List<City> filteredCities = new ArrayList<>();
        for (City city : cityList) {
            if ((state == null || city.getState().equals(state)) &&
                (region == null || city.getRegion().equals(region))) {
                filteredCities.add(city);
            }
        }
        return filteredCities;
    }

    public boolean updateCity(long id, String state, String city, String region) {
        for (City cityObj : cityList) {
            if (cityObj.getId() == id) {
                cityObj.setState(state);
                cityObj.setCity(city);
                cityObj.setRegion(region);
                return true;
            }
        }
        return false;
    }

    public boolean deleteCity(long id) {
        Iterator<City> iterator = cityList.iterator();
        while (iterator.hasNext()) {
            City city = iterator.next();
            if (city.getId() == id) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }
}