package alisson;

import java.util.List;

public class CityController {
    private CityService cityService = new CityService();

    public void createCity(String state, String city, String region) {
        cityService.createCity(state, city, region);
    }

    public List<City> getAllCities() {
        return cityService.getAllCities();
    }

    public List<City> filterCities(String state, String region) {
        return cityService.filterCities(state, region);
    }

    public boolean updateCity(long id, String state, String city, String region) {
        return cityService.updateCity(id, state, city, region);
    }

    public boolean deleteCity(long id) {
        return cityService.deleteCity(id);
    }
}
