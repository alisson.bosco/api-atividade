package alisson;

import java.io.IOException;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;

public class App {

    public static void main( String[] args ) throws IOException {
        CityController cityController = new CityController();
        CityHandler cityHandler = new CityHandler( cityController );

        HttpServer server = HttpServer.create( new InetSocketAddress( 8080 ), 0 );
        server.createContext( "/", cityHandler );
        server.setExecutor( null );
        server.start();

        System.out.println( "Server started on port 8080" );
        City newCity = new City( 1, "New State", "New City", "New Region" );
        cityController.createCity( "EUA", "New York City", "Texas" );
        System.out.println(
                "City created: " + newCity.getState() + " - " + newCity.getCity() + " - " + newCity.getRegion() );

    }
}
